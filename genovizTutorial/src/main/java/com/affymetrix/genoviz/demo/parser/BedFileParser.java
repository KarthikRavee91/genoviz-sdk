package com.affymetrix.genoviz.demo.parser;

import com.affymetrix.genoviz.demo.datamodel.BedSequence;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.io.InputStream;

public class BedFileParser {
    HashMap<String,Vector<BedSequence>> results ;
    static int map_length = 0;
    public BedFileParser(String bedFile_URL) {
        String urlpath = new File(bedFile_URL).toURI().toString();
        try {
            getAlignments(new URL(urlpath));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    public void getAlignments(URL url) {
        results = null;
        try {
            InputStream istream = url.openStream();
            results = BedFileParser.getAlignment(new BufferedReader(new InputStreamReader(istream)));
            istream.close();
        } catch (IOException ex) {
            System.out.print("BedFileParser: could not get sequence. ");
            System.out.println(ex.getMessage());
        }
    }
    public static int getMapLength(){
        return map_length;
    }
    public static HashMap<String,Vector<BedSequence>> getAlignment(BufferedReader chars) throws IOException {
        HashMap<String,Vector<BedSequence>> results = new HashMap<>();
        String line = chars.readLine();
        while(line != null){
            BedSequence bedSequence = new BedSequence();
            String[] lineSplit = line.split("\t");
            bedSequence.setChr(lineSplit[0]);
            bedSequence.setStart(Integer.parseInt(lineSplit[1]));
            bedSequence.setEnd(Integer.parseInt(lineSplit[2]));
            map_length = Math.max(map_length, Integer.parseInt(lineSplit[2]));
            bedSequence.setName(lineSplit[3]);
            bedSequence.setStrand(lineSplit[5]);
            bedSequence.setSeqG(lineSplit[10].split(","));
            bedSequence.setLaG(lineSplit[11].split(","));
            bedSequence.setDescription(lineSplit[13]);
            bedSequence.createSeqMap();
            String index = bedSequence.getChr()+bedSequence.getStrand();
            if(results.get(index) == null)
                results.put(index,new Vector<BedSequence>());
            results.get(index).add(bedSequence);
//            System.out.println(results);
            line = chars.readLine();

        }
        return results;
    }
    public HashMap<String,Vector<BedSequence>> getResults() {
        return results;
    }

//    public static void main(String[] args) {
//        BedFileParser bedFileParser = new BedFileParser("file:///C:/Users/karth/genoviz-sdk/genovizTutorial/src/main/resources/data/AraportBedFile");
//        System.out.println(bedFileParser.getResults().get("Chr5"));
//    }

}
